# renaming macro in python

Be careful, this macro may compromise metadata.
By default the prefix is numeric, by now.
Modules used: os, shutils, argparse

## How to use
Load every file you want to rename into a runit_data folder. 

Create the folders 'runit_data' and 'runit_dataR' , without quotes, on the same folder where you have runit.py

execute the runit.py file with the syntax: runit.py [-n|--name] <newname>

Your renamed files will be on runit_dataR folder.

WTFPL license, aka i don't care about what you do :).
