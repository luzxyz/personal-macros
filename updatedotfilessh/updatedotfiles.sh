#!/bin/bash
# Copy this file in your home directory, give it permissions to be executed.
# List your directories and files on the array
# change the home folder and destination folder.
# Lay back and see how other devs seethe because of your big brain move
files=(.Xresources .zshrc .bashrc .urxvt .xinitrc .config)
for i in "${files[@]}"
do
	cp -vr /home/lesser/$i /home/lesser/dot_files/
done

echo ""
echo "             ______________________________________"
echo "            |                                      |"
echo " _(:37)\_ < | Copying files ended with exit code $? |"
echo "            |______________________________________|"
echo "					Have a nice day!"
