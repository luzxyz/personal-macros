# Template shell script for maintaining a dotfiles repository

This shell scrip has been tested on bash, be sure to have the #!/bin/bash at line 1
Be free to adapt it to any shell you want, i don't think that the compatibility would be a trouble.

# How to use

Place this script on your home directory (Unix systems) (~)
Modify it for your needs.
Execute it as ./updatedotfiles.sh
Done!

This script doesnt manage the repository, you need to mantain it manually unless you want to do it on the same shell script ;)

WTFPL License
